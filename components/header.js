import React, { Component } from 'react'; 

class Header extends React.Component{

    render(){  

        return(  
            <header id="header" className="header">
                <div className="container-fluid ">
                    <div className="row">
                    <div className="col-md-2 col-sm-6 col-4">
                        <a href="" className="logo"><img src="../static/images/baja-logo.png"/></a>
                    </div>
                    <div className="col-md-8 col-sm-6 col-8">
                        <div className="menu">
                        <ul className="d-flex justify-content-between">
                            <li className="hidden768"><a href="">How to Play </a></li>
                            <li className="hidden768"><a href="">What You Win </a></li>
                            <li className="hidden768"><a href="">T&C</a></li>
                            <li className="visitors">500<span>Visitors</span></li>
                            <li className="menu-btn show768" id="toggle"><i className="fa fa-bars" aria-hidden="true"></i></li>
                        </ul>
                        </div>
                    </div>
                    <div className="col-md-2 col-sm-12 text-center">
                        <div className="just-emi-imge ">
                        <img src="../static/images/just-emi-imge.png"/>
                        <p className="cntst-date">1st – 28th Oct’19</p>
                        </div>
                    </div>
                    <div className="overlay-menu">
                        <div className="close-btn text-right"><a href="javascript:;"><i className="fa fa-times" aria-hidden="true"></i></a></div>
                        <div className="mobile-menu">
                        <ul>
                            <li className=""><a href="">How to Play </a></li>
                            <li className=""><a href="">What You Win </a></li>
                            <li className=""><a href="">T&C</a></li>
                        </ul>
                        </div>
                    </div>
                    </div>
                </div>
            </header>
         );
    }
}



 export default Header ;