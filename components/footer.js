import React, { Component } from 'react'; 

class Footer extends React.Component{

    render() {
        return (
                <footer>
                <div className="container">
                    <div className="row flex-wrap-reverse">
                    <div className="col-md-8 col-sm-8 col-12">
                        <div className="right-reserve">
                        <p>All rights reserved © 2019 Bajaj Finance Limited   |   <a href="">t&c</a> apply</p>
                        </div>
                    </div>
                    <div className="col-md-4 col-sm-4 col-12">
                        <div className="social-inks text-right">
                        <ul>
                            <li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        </ul>
                        </div>
                    </div>
                    </div>
                </div>
                </footer>
             );
        }
}
export default Footer;