import React from 'react'
import Head from 'next/head'
import Header from '../components/header'
import Footer from '../components/footer'

class Riddel extends React.Component{

    constructor(props)
        {
            super(props);
            this.state = {
             Question : [
                    [  
                        {
                            'Question':'',
                            'Answer':'',
                            'Option1':'',
                            'Option2':'',
                            'Option3':'',
                            'Option4':'',
                        },
                       
                        
                        
                    ],
                    
                 ]
          }
        }
    

    render()
    {
        let item = this.state.truefalse[Math.floor(Math.random()*items.length)];
        return (
         <section className="game-page-section">
            <div className="container-fluid text-center zindex-2">
            <div className="row">
                <div className="game-heading">
                <h2>WHAT’S ON EMI?</h2>
                <p>Guess if the product is on EMI and stand a chance to win</p>
                <a href="" className="back-to-home"><span><i class="fa fa-arrow-left" aria-hidden="true"></i></span>Back to Home</a>
                </div>
                <div className="clearfix"></div>
                <div className="col-md-12">
                <div className="game-1">
                    <div className="tinder">
                        <div className="tinder--status">
                        <i className="fa fa-remove"></i>
                        <i className="fa fa-heart"></i>
                        </div>

                        <div className="tinder--cards d-flex justify-content-center ">
                        {
                            item.map((value, index) => 
                                <div className="tinder--card">
                                    <div className="product-bg">
                                    <img src={value.images} alt=""/>
                                    </div> 
                                </div>
                            )
                        }

                       
                        
                        </div>
                        <div className="tinder--buttons">
                        <div className="btn-wrap">
                            <button id="nope"><img src="images/dis-like.png" alt=""/>
                            </button>
                            <p>Not On EMI</p>
                        </div>
                        <div className="btn-wrap">
                            <button id="love"><img src="images/like.png" alt=""/> 
                            </button>
                            <p>On EMI</p>
                        </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </section>
        );
    }



}