import React from 'react'
import App from 'next/app'
import Head from 'next/head'
import Router from 'next/router'

import Header from '../components/header'
import Footer from '../components/footer'




export default class MyApp extends App {
   
    
  
    render () {
      const { Component, pageProps } = this.props
      
      return (
          <div>
            <Head>
              <title>Bajaj</title>
              <meta name="viewport" content="initial-scale=1.0, width=device-width" />
              <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" />
                
                <link href="../static/css/font-awesome.css" rel="stylesheet"/>
                <link href="../static/css/font-awesome.css" rel="stylesheet"/>
                <link href="../static/css/style.css" rel="stylesheet"/>
                <link href="../static/css/responsive.css" rel="stylesheet"/>
           </Head>
            <div>
              <Header/>
                <Component Component {...pageProps} />
              <Footer />
            </div>
          </div>
      )
    }
  }