import React from 'react'
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';


class  Home extends  React.Component{
  constructor (props)
   {
      super(props);
            this.state = {

               gameScreen : [
               {
                  'images':'../static/images/first-game.png',
                  'title':'WHAT’S ON EMI?'
               },
               {
                  'images':'../static/images/second-game.png',
                  'title':'Flip the card'
               },
               {
                  'images':'../static/images/three-game.png',
                  'title':'Who am I?'
               },
               {
                  'images':'../static/images/four-game.png',
                  'title':'FIND EMI PRODUCTS'
               },
               
            ]
         }
   }

  
    
     render(){
      return(
        
         <section className="home-page-main-section">
            <div className="game-type">
            <div className="container zindex-2">
               <div className="row">
               <div className="homepage-heading text-center">
                  <h1>4 Games,<br/> 200 Winners</h1>
               </div>
             
               <OwlCarousel className="owl-carousel owl-theme home-page-carousel" margin={10}>
               { this.state.gameScreen.map((value, index) => 
                     <div className="item">
                        <div className="single-game-wrap">
                           <div className="game-image">
                              <img src={ value.images} alt={ value.title }/>
                        </div>
                        <div className="game-title text-center">
                        <p>{ value.title }</p>
                        </div>
                        <div class="game-btns text-center">
                        <a href="" className="active" data-toggle="modal" data-target="#login-form">Play Now</a>
                        <a href="" onClick="this.howtoPlay()">How To Play</a>
                        </div>
                     </div>
                  </div>

                )}
                </OwlCarousel>
                  
               </div>
            </div>
            </div>
            
         </section>
         
      )
     }
   

  }


export default Home
